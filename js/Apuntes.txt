//Nombre de Variables, tipos de sintaxis:
primerNombre // Camelcase
primer_nombre // underscore
PrimerNombre // Pascal Case

//Tipo de Datos Primitivo
Symbol('SImbolo)

//Template Literals Muy bueno el llamado a la funcion desde el template.
`Bienvenido ${nombre} de edad ${getEdad)=}`;

//Tipo de Funciones
IIFE --» Son invocadas inmediatamente 
(function(parametro){
    //pass
})('Valor Parametro');

//forEach
const tareas = [...];
tareas.forEach(function(tarea,index){
    //pass
});

//map
const carrito = [
    {id:1,producto='algo'},
    {id:2,producto='algo2'}
    {id:3,producto='algo3'}
    {id:4,producto='algo4'}
];
cosnt nombreProd = carrito.map(funcion (carrito){
    return carrito.producto;
})

//Alternativa a map 
for(let producto in carrito) {
    //pass;
}

//default,entries, values y keys iterador
for(let entrada of datos.values() ){
    //pass entrada
}
for(let entrada of datos.entries() ){
    //pass entrada
}
for(let entrada of datos.keys() ){
    //pass entrada
}
for(let entrada of datos){
    //pass entrada
}

//Windows object
prompt, confirm,

//Redireccionar y moverce por el historial  del  navegador
windows.history.go(index)

####3. DOM, WINDOWS Y SCRIPTING
//pasar de coleccion a array
const links = documennt.getElementsByTagName
let enlaces = Array.from(links);
enlaces.forEach(funcion(enlace){
    //enlace.textContent;
})

//Otra forma de seleciconar elementos
document.querySelectorAll('#idelement .algodentro')

//Node Types
1 = elementos
2 = Atributo
3 = Text Node 
8 = Comentarios
9 = Documentos
10 = Doctype

//Traversing y Delegations MUY BUENO
document.body.addEventListener('click',eliminarElemento);

function eliminarElemento(e){
    e.preventDefault();
    if(e.target.classList.contains('borrar-curso')){
        e.target.parentElement.parentElement.remove();
    }
}
